Add icons to your links and creates small images for them according to there link extensions.

![Picture of the recipe](http://www.pmwiki.org/pmwiki/uploads/Cookbook/linkicons.png)


## Questions Answered By This Recipe

How can I easily display the type of links like PDF, HTML with a little icon according to the type and can use the
icons for external links while internal links of my wiki are displayed in *normal* style?


## Description

This recipe simply add icons to your links. Just follow the instruction of the installation.


## Installation

- download from [source][source] or via git `git clone
  git@bitbucket.org:wikimatze/pmwiki-linkicons-recipe.git`
- unzip everything
- make the following copy-tasks:
    - put `linkicons.php` to your cookbook directory
    - put `linkicons` directory in your public directory (normally _pub_)
    - put `linkicons.css` in your CSS directory (normally _pub/css_)
- now you should have the following structure
    - *cookbook/linkicons.php*
    - *pub/linkicons*
    - *pub/css/linkicons.css*
- add to *local/config.php* in the following order:
    - `$UrlLinkFmt= "<a class='external' href='\\$LinkUrl' rel='nofollow'\>\\$LinkText</a\>";`
    - `include_once("$FarmD/cookbook/linkicons.php");`


## Notes

Credits goes to [Jeremie Tisseau][Jeremie]. The icons are from [FamFamFam][Fam]. This recipe doesn't work under
Internet-Explorer - feel free to post the CSS-hacks to get it working. To enable the upload of SVG's you need to add
`$UploadExts['svg'] = 'image/svg';` in your *config.php*.


## See Also

- [External Links][External 1] Configure external links to open in a new window, have a "tool tip
  title", or use other CSS classes.
- [External Links2][External 2] Add and icon to external links and make them automatically open in a
  new window.
- [Attach Icons][Attach 1] Add icon images to Attach file links according to file extension.
- [Attach Icons2][Attach 2] Add icon images to Attach file links according to file extension.
- [Custom Bullets][Custom Bullets] Enable custom bullets to distinguish document types.


## Comments/Feedback

See [Linkicons-Talk][talk] - your comments are welcome there!


## Contact

Feature request, bugs, questions, and so on can be send to <matthias.guenther@wikimatze.de>. If you enjoy the script
please leave your comment under [Linkicons Users][Linkicons Users].


## License

This software is licensed under the [MIT license] [mit].

© 2011-2013 Matthias Guenther <matthias.guenther@wikimatze.de>.

[Attach 1]: http://www.pmwiki.org/wiki/Cookbook/AttachIcons/
[Attach 2]: http://www.pmwiki.org/wiki/Cookbook/AttachIcons2/
[Custom Bullets]: http://www.pmwiki.org/wiki/Cookbook/CustomBullets/
[External 1]: http://www.pmwiki.org/wiki/Cookbook/ExternalLinks/
[External 2]: http://www.pmwiki.org/wiki/Cookbook/ExternalLinks2/
[Fam]: http://www.famfamfam.com/
[Jeremie]: http://web-kreation.com/index.php/html-css/add-file-type-icons-next-to-your-links-with-css/
[Linkicons Users]: http://www.pmwiki.org/wiki/Cookbook/LinkIcons-Users/
[mit]: http://en.wikipedia.org/wiki/MIT_License/
[source]: https://bitbucket.org/wikimatze/pmwiki-linkicons-recipe/overview/
[talk]: http://www.pmwiki.org/wiki/Cookbook/LinkIcons-Talk/


/* vim: set ts=2 sw=2 textwidth=120: */
